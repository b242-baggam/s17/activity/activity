/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

function userInfo(){
	let name = prompt("What is your name?");
	let age = prompt("How old are you?");
	let location = prompt("Where do you live?");
	alert("Thank you for your input!");
	console.log("Hello, " + name);
	console.log("You are " + age + " years old.");
	console.log("You live in " + location + " City");
}
userInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
function favArtists(){
	console.log("1. Arijit Singh");
	console.log("2. Armaan Malik");
	console.log("3. Pritam");
	console.log("4. Darshan Raval");
	console.log("5. Sid Sriram");
}
favArtists();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
function FavMoviesRatings(){
	console.log("1. Jersey");
	console.log("Rotten Tomatoes Rating: 3.5/5");
	console.log("2. Ala Vaikhuntapuram lo");
	console.log("Rotten Tomatoes Rating: 3.5/5");
	console.log("3. MS Dhoni");
	console.log("Rotten Tomatoes Rating: 75%");
	console.log("4. Nuvvu Naaku Nachhav");
	console.log("Rotten Tomatoes Rating: 75%");
	console.log("5. Geetha Govindam");
	console.log("Rotten Tomatoes Rating: 61%");
}
FavMoviesRatings();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();


// console.log(friend1);
// console.log(friend2);
